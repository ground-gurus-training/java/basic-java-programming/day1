package com.groundgurus.day1;

public class Variables {
    public static void main(String[] args) {
        // long version
        int age; // defined age variable
        age = 34; // assigned age value to age variable
        //int age = 34; // shorthand/shortcut

        double salary; // defined age variable
        salary = age * 10.4; // assigned an expression to age variable

        System.out.println("Your daily salary is " + salary + " USD");
    }
}
