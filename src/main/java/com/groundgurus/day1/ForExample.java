package com.groundgurus.day1;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class ForExample {
    public static void main(String[] args) {
        // var is used to simply the type definition of a variable
        // statically type variables = Java, C#, Kotlin (Compiled Languages)
        // the type is determined during compilation

        // in Java, var keyword can only be used in local variables

        // dynamically type variables = JavaScript, Python, PHP (Dynamic Languages)
        // the type is determined during runtime

        String[] days = { "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun" };

        // Enhanced for loop
        for (var day : days) {
            System.out.println(day);
        }

        System.out.println();

        // Traditional for loop
        for (var i = days.length - 1; i >= 0; i -= 2) {
            System.out.println(days[i]);
        }
    }
}
