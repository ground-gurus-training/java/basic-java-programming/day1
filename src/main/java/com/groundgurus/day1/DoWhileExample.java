package com.groundgurus.day1;

import java.util.Random;
import java.util.Scanner;

public class DoWhileExample {
    public static void main(String[] args) {
        Random rnd = new Random();
        Scanner in = new Scanner(System.in);
        int dice;

        System.out.print("Enter your guess: ");
        int guess = in.nextInt();

        do {
            dice = rnd.nextInt(6) + 1;

            if (dice != guess) {
                System.out.println("We rolled " + dice + " Let's keep rolling!");
            }
        } while (dice != guess);

        System.out.println("Got it!");
    }
}
