package com.groundgurus.day1.exercises;

import java.util.Scanner;

public class Day1Exercise1 {
    public static void main(String[] args) {
        var scanner = new Scanner(System.in);

        System.out.print("Enter first number: ");
        var firstNumber = scanner.nextDouble();

        System.out.print("Enter second number: ");
        var secondNumber = scanner.nextDouble();

        // pwedeng ganito
        double sum = firstNumber + secondNumber;
        System.out.println("The sum is " + sum);

        // or pwedeng ganito
        System.out.println("The difference is " + (firstNumber - secondNumber));
        System.out.println("The product is " + (firstNumber * secondNumber));
        System.out.println("The quotient is " + (firstNumber / secondNumber));
    }
}
