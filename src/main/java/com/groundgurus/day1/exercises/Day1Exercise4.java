package com.groundgurus.day1.exercises;

import java.util.Scanner;

public class Day1Exercise4 {
    public static void main(String[] args) {
        var scanner = new Scanner(System.in);

        System.out.print("Enter width: ");
        var width = scanner.nextInt();

        System.out.print("Enter height: ");
        var height = scanner.nextInt();

        printRectangle(width, height);
    }

    private static void printRectangle(int width, int height) {
        for (var i = 1; i <= height; i++) {
            for (var j = 1; j <= width; j++) {
                // this is the challenge solution for this exercise
                if ((i > 1 && j > 1) && (i != height && j != width)) {
                    System.out.print(" ");
                } else {
                    System.out.print("*");
                }

                if (j < width) {
                    System.out.print(" ");
                }

// This is the simple solution for this exercise
//                System.out.print("*");
//
//                if (j < width) {
//                    System.out.print(" ");
//                }
            }
            System.out.println();
        }
    }
}
