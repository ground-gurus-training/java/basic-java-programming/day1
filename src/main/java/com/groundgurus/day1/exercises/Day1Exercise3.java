package com.groundgurus.day1.exercises;

import java.util.Scanner;

public class Day1Exercise3 {
    public static void main(String[] args) {
        var scanner = new Scanner(System.in);

        System.out.print("Enter first number: ");
        var firstNumber = scanner.nextInt();

        System.out.print("Enter second number: ");
        var secondNumber = scanner.nextInt();

        if (firstNumber > secondNumber) {
            System.out.println(firstNumber + " is higher than " + secondNumber);
        } else {
            System.out.println(secondNumber + " is higher than " + firstNumber);
        }
    }
}
