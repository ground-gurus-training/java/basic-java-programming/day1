// package declaration
// optional, can have up to 1 package declaration
package com.groundgurus.day1;

// import statements
// optional, can have to up x number of import declarations
import java.text.SimpleDateFormat;
import java.util.Date;

// class declaration
public class WhatIsTheDateToday {
    public static void main(String[] args) {
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        System.out.println("Today is " + sdf.format(new Date()));
    }
}
